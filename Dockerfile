FROM node:13.5.0-alpine3.11

RUN addgroup -g 501 ec2-user \
    && adduser -u 501 -G ec2-user -s /bin/sh -D ec2-user \
    && apk add git \
    && npm install -g gatsby-cli
    
USER ec2-user
